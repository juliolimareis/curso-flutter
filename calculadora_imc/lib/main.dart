import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  String _info = "";

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _resetFilelds(){
    weightController.text = "";
    heightController.text = "";
    setState(() {
      _info = "";
      _formKey = GlobalKey<FormState>();
    });
  }

  void _calculate(){
    setState(() {
      double weight = double.parse(weightController.text);
      double heigh = double.parse(heightController.text) / 100;
      double imc = weight / (heigh*heigh);

      if(imc < 18){
        _info = "Abaixo do Peso (${imc.toStringAsPrecision(3)})";
      }else if(imc >= 18 && imc <= 24.9){
        _info = "Peso normal (${imc.toStringAsPrecision(3)})";
      }else if(imc > 24.9 && imc <= 29.9){
        _info = "Sobrepeso ( ${imc.toStringAsPrecision(3)})";
      }else if(imc >= 29.9 && imc <= 34.9 ){
        _info = "Obesidade grau I (${imc.toStringAsPrecision(3)})";
      }else if(imc > 34.9 && imc <= 39.9 ){
        _info = "Obesidade grau II (${imc.toStringAsPrecision(3)})";
      }else if(imc > 39.9){
        _info = "Obesidade mórbida (${imc.toStringAsPrecision(3)})";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Calculadora IMC"),
        centerTitle: true,
        backgroundColor: Colors.green,
        actions: <Widget>[

          IconButton(
            iconSize: 40.0,
            icon:Icon(Icons.refresh),
            onPressed:this._resetFilelds,
          )
        ],
      ),

      body:SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
        child: Form(
          key: _formKey,
          child:Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[

                Icon(
                    Icons.person_outline,
                    color:Colors.green,
                    size:120.0
                ),

                TextFormField(
                  controller: weightController,
                  keyboardType: TextInputType.number,
                  // ignore: missing_return
                  // TODO função de validação
                  validator:(value){
                    if(value.isEmpty){
                      return "Insira o peso";
                    }
                  },
                  decoration: InputDecoration(
                      labelText: "Peso (kg)",
                      labelStyle: TextStyle(
                        color:Colors.green,
                      )
                  ),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.green,
                      fontSize: 25.0
                  ),
                ),

                TextFormField(
                  controller: heightController,
                  keyboardType: TextInputType.number,
                  // ignore: missing_return
                  validator:(value){
                    if(value.isEmpty){
                      return "Insira o altura";
                    }
                  },
                  decoration: InputDecoration(
                      labelText: "Altura (cm)",
                      labelStyle: TextStyle(
                        color:Colors.green,
                      )
                  ),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.green,
                      fontSize: 25.0
                  ),
                ),

                Padding(
                  padding: EdgeInsets.only(top:20.0),
                  child:Container(
                      height: 50.0,
                      child:RaisedButton(
                        child: Text("Calcular", style: TextStyle(
                            color:Colors.white,
                            fontSize: 25.0
                        )),
                        color:Colors.green,
                        onPressed:(){
                          //todo Verifica se é valido
                          if(_formKey.currentState.validate()) {
                            this._calculate();
                          }
                        },
                      )
                  ),
                ),

                Text(this._info,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color:Colors.green,
                        fontSize: 25.0
                    )
                )
              ]),

        )
      )
    );
  }
}
