# Estudos em Flutter

## Números decimais no iOS

#### Para exibir o botão de "." (ponto decimal) no teclado do iOS, devemos realizar uma pequena alteração:

Substitua o comando
```dart
keyboardType: TextInputType.number,
```
por
```dart
keyboardType: TextInputType.numberWithOptions(decimal: true),
```

Desta forma você poderá digitar números decimais no iOS.

## Gerar chave SH1 no Linux
Executar comando em android-studio/jre/bin/
```sh
keytool -list -v -alias androiddebugkey -keystore ~/.android/debug.keystore -storepass android -keypass android
```
Configuração de Autenticação na google

url: https://console.developers.google.com


