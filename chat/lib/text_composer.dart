import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class TextComposer extends StatefulWidget {

  TextComposer(this.sendMessage);

  final Function({String text, File imgFile}) sendMessage;

  @override
  _TextComposerState createState() => _TextComposerState();
}

class _TextComposerState extends State<TextComposer> {
  bool _isComposing = false;
  Color _colorSend = Colors.grey;

  final TextEditingController _controler = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      child: Row(
        children: <Widget>[

          IconButton(
            icon: Icon(Icons.photo_camera),
            onPressed: () async {
              final File imgFile = await ImagePicker.pickImage(
                  source: ImageSource.camera
              );

              if(imgFile == null) return;

              widget.sendMessage(imgFile: imgFile);


            },
          ),

          Expanded(
            child: TextField(
              controller: _controler,
              decoration: InputDecoration.collapsed(
                  hintText: "Enviar mensagem"
              ),
              onChanged: (text){
                setState(() {
                  _isComposing = text.isNotEmpty;
                  _activeColorSend();
                });
              },
              onSubmitted: (text){
                widget.sendMessage(text: text);
                _reset();
              },
            ),
          ),

          IconButton(
            icon: Icon(Icons.send, color: _colorSend),
            onPressed: _isComposing ? (){
              widget.sendMessage(text: _controler.text);
              _reset();
            }: (){

            }
          )

        ],
      ),
    );
  }

  void _reset(){
    _controler.clear();
    setState(() {
      _isComposing = false;
      _activeColorSend();
    });
  }

  void _activeColorSend(){
    if(_isComposing){
      _colorSend = Colors.green;
    }else{
      _colorSend = Colors.grey;
    }
  }

}


