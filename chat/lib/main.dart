import 'package:chat/chat_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

/*
* Gerar chave SHA1
* comando: keytool -list -v -alias androiddebugkey -keystore ~/.android/debug.keystore -storepass android -keypass android
*
* Executar comando em android-studio/jre/bin/
*
* */
void main() async {
  runApp(MyApp());

  //obter atualização em tempo real
//  Firestore.instance.collection("mensagens").document('S6Q6Rm3CPnM4nGIviEHk').snapshots().listen((snapshot){
////    snapshots.documents.forEach((data){
//      print(snapshot.data);
////    });
//  });

  //obter todos dados
//  QuerySnapshot snapshot = await Firestore.instance.collection("mensagens").getDocuments();
//  snapshot.documents.forEach((data){
//    print(data.data);
//  });

//  //obter todos dados e atualizar dados
//  QuerySnapshot snapshot = await Firestore.instance.collection("mensagens").getDocuments();
//  snapshot.documents.forEach((data){
//    data.reference.updateData({
//      'read': true
//    });
//  });

//  //obter dados especificos
//  DocumentSnapshot snapshot = await Firestore.instance.collection("mensagens")
//      .document('S6Q6Rm3CPnM4nGIviEHk').get();
//  print(snapshot.data);


    //guardar dados.
//  Firestore.instance.collection("mensagens").document('S6Q6Rm3CPnM4nGIviEHk')
//      .collection('files').document().setData( //updateData
//    {
//      'file':'foto.png'
//    }
//  );



}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        iconTheme: IconThemeData(
//          color:Colors.blue
        )
      ),
      home: ChatScreen(),
    );
  }
}

