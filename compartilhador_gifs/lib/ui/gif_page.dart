import 'package:flutter/material.dart';
import 'package:share/share.dart';

class Gif extends StatelessWidget {

  final Map _gifData;
  //construtor
  Gif(this._gifData);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(_gifData["title"]),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share),
            onPressed: (){
              Share.share(_gifData["images"]["fixed_height"]["url"]);
            },
          )
        ],
      ),

      body: Center(
        child: Image.network(_gifData["images"]["fixed_height"]["url"]),
      ),

    );
  }
}
