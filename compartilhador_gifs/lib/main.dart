import 'package:compartilhadorgifs/ui/home_page.dart';
import 'package:flutter/material.dart';

/* Beast: https://api.giphy.com/v1/gifs/trending?api_key=CXwyMEnv0MeEXsHqh8KI4IIeZw8rsUVf&limit=20&rating=G
*  Search:
* */

void main(){
  runApp(MaterialApp(
    home: HomePage(),
    theme: ThemeData(hintColor: Colors.white),
  ));
}