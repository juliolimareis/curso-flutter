import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

//TODO variáveis da tabela
final String contactTable = "ContactTable";
final String idColumn = "idColumn";
final String nameColumn = "nameColumn";
final String emailColumn = "emailColumn";
final String phoneColumn = "phoneColumn";
final String imgColumn = "imgColumn";

class ContactHelper{

  //TODO criar objeto global Singleton
  static final ContactHelper _instanse = ContactHelper.internal();
  factory ContactHelper() => _instanse;
  ContactHelper.internal();

  Database _db;

  //TODO inicia banco de dados
  Future <Database> get db async {
    if(_db != null){
      return _db;
    }else{
      _db = await initDb();
      return _db;
    }
  }

  Future<Database>initDb() async {
    final databasesPath = await getDatabasesPath();
    final path = join(databasesPath, "contacts.db");

    return await openDatabase(path, version: 1, onCreate: (Database db, int newerVersion) async {
      await db.execute(
        "CREATE TABLE $contactTable("
            "$idColumn INTEGER PRIMARY KEY, "
            "$nameColumn TEXT, "
            "$emailColumn TEXT,"
            "$phoneColumn TEXT,"
            "$imgColumn TEXT"
        ")"
      );
    });
  }

  Future<Contact> saveContact (Contact contact) async {
    //instanciando conexão
    Database dbContact = await db;
    contact.id = await dbContact.insert(contactTable, contact.toMap());
    return contact;
  }

  Future<Contact> getContact (int id) async {
    Database dbContact = await db;
    List<Map> maps = await dbContact.query(contactTable,
      columns: [idColumn, nameColumn, emailColumn, phoneColumn, imgColumn],
      where: "$idColumn = ?",
      whereArgs: [id]
    );
    if(maps.length > 0){
      return Contact.fromMap(maps.first);
    }else{
      return null;
    }
  }

  Future<List> getAllContacts () async {
    Database dbContact = await db;
    List<Map> listMap = await dbContact.rawQuery("SELECT * FROM $contactTable");
    List<Contact> lisContact = List();
    for(Map map in listMap){
      lisContact.add(Contact.fromMap(map));
    }
    return lisContact;
  }

  Future<int> getNumber () async {
    Database dbContact = await db;
    return Sqflite.firstIntValue(await dbContact.rawQuery("SELECT COUNT(*) FROM $contactTable"));
  }

  Future close() async {
    Database dbContact = await db;
    dbContact.close();
  }

  Future<int> updateContact (Contact contact) async {
    //instanciando conexão
    Database dbContact = await db;
    return await dbContact.update(contactTable,
      contact.toMap(),
      where: "$idColumn = ?",
      whereArgs: [contact.id]
    );
  }

  Future<int> deleteContact (int id) async {
    Database dbContact = await db;
    return await dbContact.delete(contactTable,
      where: "$idColumn = ?",
      whereArgs: [id]
    );
  }



}

class Contact{
  int id;
  String name;
  String email;
  String phone;
  String img;

  Contact();

  Contact.fromMap(Map map){
    id = map[idColumn];
    name = map[nameColumn];
    email = map[emailColumn];
    phone = map[phoneColumn];
    img = map[imgColumn];
  }

  Map toMap(){
    Map<String, dynamic> map = {
      nameColumn: name,
      emailColumn: email,
      phoneColumn: phone,
      imgColumn: img
    };
    if(id != null){
      map[idColumn] = id;
    }
    return map;
  }

  //CTR+O
  @override
  String toString() {
    return "Contact(id: $id, name: $name, email: $email, phone: $phone, img: $img)";
  }
}