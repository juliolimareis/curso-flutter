import 'package:flutter/material.dart';

void main (){
  runApp(
      MaterialApp(
        title:"Contador de pessoas",
        home:Home()
      )
  );
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _people = 0;
  String _msg = "";

  void setMsg(msg){
    _msg = msg;
  }

  void _changePeople(int delta){
    //atualiza a tela
    setState((){
      _people += delta;
      if(_people > 10){
        setMsg('Restaurante esta sem vagas');
      }else if(_people < 0){
        setMsg('Mundo inverso?');
      }
      else{
        setMsg('Vagas disponíveis');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Image.asset(
          'img/restaurant.jpg',
          fit: BoxFit.cover,
          height:double.maxFinite,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Text(
                "Pessoas: $_people",style:TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold
            )),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child:FlatButton(
                    child:
                    Text("+1",style: TextStyle(
                        fontSize: 40.0,
                        color:Colors.white
                    )),
                    onPressed: (){
                      _changePeople(1);
                    },
                  ),
                ),

                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: FlatButton(
                    child:
                    Text('-1',style: TextStyle(
                        fontSize: 40.0,
                        color:Colors.white
                    )),
                    onPressed: (){
                      _changePeople(-1);
                    },
                  ),
                ),

              ],
            ),

            Padding(
              padding: EdgeInsets.fromLTRB(25.0,0,25.0,0),
              child: Text(_msg,style:TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
                fontSize: 30.0,

              ))
            ),



          ],
        )
      ],
    );
  }
}
