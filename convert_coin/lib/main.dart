import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';

const request = 'https://api.hgbrasil.com/finance?format=json&key=846ef9de';

Future<Map> getData() async {
  var response = await http.get(request);
  return json.decode(response.body);
}

void main() async {
  runApp(MaterialApp(
    home:Home()
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  final realController = TextEditingController();
  final dolarController = TextEditingController();
  final euroController = TextEditingController();

  double dolar;
  double euro;


  void _realChanged(String text){
    double coin = double.parse(text);
    dolarController.text = (coin/dolar).toStringAsFixed(2);
    euroController.text = (coin/euro).toStringAsFixed(2);
  }

  void _dolarChanged(String text){
    double coin = double.parse(text);
    realController.text = (coin*dolar).toStringAsFixed(2);
    euroController.text = (coin*dolar/euro).toStringAsFixed(2);
  }

  void _euroChanged(String text){
    double coin = double.parse(text);
    realController.text = (coin*euro).toStringAsFixed(2);
    dolarController.text = (coin*euro/dolar).toStringAsFixed(2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text('\$ Converter moedas \$', style:TextStyle(
            color:Colors.black,
          )
        ),
        backgroundColor: Colors.amber,
        centerTitle: true,
      ),

      body: FutureBuilder<Map>(
        future: getData(),
        builder: (context, snapshot){
          switch(snapshot.connectionState){
            case ConnectionState.none:

            case ConnectionState.waiting:
              return Center(
                child:Container(
                  color: Colors.black,
                  child: Center(
                    child: Loading(indicator: BallPulseIndicator(), size: 50.0,color: Colors.amber),
                  ),
                ),
//                  Text(
//                    'Consultando Valores ....',
//                    textAlign:TextAlign.center,
//                    style: TextStyle(
//                  color:Colors.amber,
//                  fontSize: 25.0
//                )),
              );

            default:
              if(snapshot.hasError){
                return Center(
                  child:Text(
                      'Erro ao obter dados :(',
                      textAlign:TextAlign.center,
                      style: TextStyle(
                          color:Colors.amber,
                          fontSize: 25.0
                      )),
                );

              }else{
                dolar = snapshot.data["results"]["currencies"]["USD"]["buy"];
                euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];

                return SingleChildScrollView(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[

                      Icon(
                        Icons.monetization_on,
                        size: 150.0,
                        color: Colors.amber,
                      ),

                      buildTexField("Real", "R\$: ", realController, _realChanged),
                      Divider(),
                      buildTexField("Dólar", "US\$: ", dolarController, _dolarChanged),
                      Divider(),
                      buildTexField("Euro", "€\$: ", euroController, _euroChanged)

                    ],
                  ),
                );
              }
          }
        },
      ),
    );
  }
}

Widget buildTexField(String label, String prefix, TextEditingController controller, Function function){
  return TextField(
    keyboardType: TextInputType.number,
    onChanged: function,
    controller: controller,
    decoration: InputDecoration(
      labelText: label,
      hintStyle: TextStyle(color: Colors.amber),
      labelStyle: TextStyle(
          color: Colors.amber
      ),
      enabledBorder:OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white)
      ),
      focusedBorder:OutlineInputBorder(
          borderSide: BorderSide(
              color: Colors.amber
          )
      ),
      prefixText: prefix
    ),
    style: TextStyle(
        color:Colors.amber,
        fontSize: 25.0
    )
  );
}


